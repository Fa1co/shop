<header>
    <div class="top-nav container">
        <div class="logo"><a href="{{route('Landing-page')}}">Falco Shop</a></div>
        @if (! request()->is('checkout'))
        <ul>
            <li><a href="{{route('shop.index')}}">{{trans('lang.Shop')}} </a></li>
            <li><a href="#">{{trans('lang.About us')}}</a></li>
            <li><a href="{{route('post.index')}}">Blog</a></li>
            <!-- Authentication Links -->
            <!-- Authentication Links -->
            @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{trans('lang.Login')}}</a>
                </li>
                @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">{{trans('lang.Register')}}</a>
                    </li>
                @endif
            @else
                <li class="nav-item">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="{{route('profile')}}" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                                 document.getElementById('logout-form').submit();">{{trans('lang.Logout')}}</a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
                </li>
            @endguest
            <li><a href="{{route("cart.index")}}">{{trans('lang.Cart')}} <span class="cart-count"><span>
                            @if (Cart::instance('default')->count()>0)
                            {{Cart::instance('default')->count()}}
                                @endif
                        </span></span></a></li>
        </ul>
        @endif
    </div>
</header>
