@extends('layout')

@section('title',  $post->title )

@section('extra-css')



@endsection

@section('content')

    <div class="breadcrumbs">
        <div class="container">
            <a href="{{route('Landing-page')}}">{{trans('lang.Home')}}</a>
            <i class="fa fa-chevron-right breadcrumb-separator"></i>
            <a href="{{route('post.index')}}">Blog</a>
            <i class="fa fa-chevron-right breadcrumb-separator"></i>
            <span>{{$post->title}}</span>
        </div>
    </div>

    @if(session()->has('success_message'))
        <div class="alert alert-success" role="alert">
            {{session()->get('success_message')}}
        </div>
    @endif

    <div class="container">

        <div>
             <h2>{{$post->title}}</h2>
            <div class="post_image">
                <img src="{{productImage($post->image)}}">
            </div>
            <div class="post_body">
                {!! $post->body !!}
            </div>
        </div>

        <div class="text-center button-container">
        <a href="{{route('post.index')}}" class="button">Back</a>
    </div>


                <div id="tag_container">
                    @include('post_presults')
                </div>
                @if (auth()->user())
                    <form action="{{route('post_comment.store')}}" method="post">
                        <div class="containers">
                            {{csrf_field()}}
                            <textarea name="data" id="editor"></textarea>
                            <input type="hidden" name="id" value="{{$post->id}}">
                        </div>
                        <button type="submit" class="button"> {{trans('lang.Comment')}}</button>
                    </form>
                @else
                    <p>
                        {{trans('lang.You must be logged in to post a comment.')}}
                    </p>
                    @endif
    </div>
    <!--coment sections -->


@endsection


@section('extra-js')
    <script src="https://cdn.ckeditor.com/ckeditor5/12.2.0/classic/ckeditor.js"></script>

    <script>
        ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
                console.error( error );
            } );
    </script>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript">
        $(window).on('hashchange', function() {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 0) {
                    return false;
                }else{
                    getData(page);
                }
            }
        });

        $(document).ready(function()
        {
            $(document).on('click', '.pagination a',function(event)
            {
                event.preventDefault();

                $('li').removeClass('active');
                $(this).parent('li').addClass('active');

                var myurl = $(this).attr('href');
                var page=$(this).attr('href').split('page=')[1];
                getData(page);
            });

        });

        function getData(page){
            $.ajax(
                {
                    url: '?page=' + page,
                    ///url: url,
                    type: "get",
                    datatype: "html",
                }).done(function(data){
                $("#tag_container").empty().html(data);
                location.hash = page;
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                alert('No response from server');
            });
        }
    </script>

@endsection
