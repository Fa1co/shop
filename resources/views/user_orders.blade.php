@extends('layout')

@section('title',  auth()->user()->name )

@section('extra-css')



@endsection

@section('content')
    <div class="breadcrumbs">
        <div class="container">
            <a href="{{route('Landing-page')}}">{{trans('lang.Home')}}</a>

            <i class="fa fa-chevron-right breadcrumb-separator"></i>
            <span>{{auth()->user()->name}}</span>
        </div>
    </div> <!-- end breadcrumbs -->

    @if(session()->has('success_message'))
        <div class="alert alert-success" role="alert">
            {{session()->get('success_message')}}
        </div>
    @endif

    <div class="products-section container">
        <div class="sidebar">
            <ul>
                <li><a href="{{route('profile')}}">My profile</a></li>
                <li><a href="{{route('orders')}}">My orders</a></li>
            </ul>
        </div> <!-- end sidebar -->


        <div>
                <h2>My Orders</h2>
                   <table class="table">
                        <tr>
                            <th>Email</th>
                            <th>Address</th>
                            <th>Price</th>
                            <th>Product in order</th>
                        </tr>
                        @foreach ($orders as $order)
                            @if($order->shipped)

                                <tr class="alert alert-success" >
                                    <td>{{$order->billing_email}} </td>
                                    <td>{{$order->billing_address.','.$order->billing_city.','.$order->billing_province}} </td>
                                    <td>{{presentPrice($order->billing_total)}} </td>
                                    <td>@foreach ($products as $product)
                                            @if($order->id == $product->order_id)
                                                {{getProduct($product->product_id).','}}
                                            @endif
                                        @endforeach</td>
                                </tr>

                            @else
                        <tr>
                            <td>{{$order->billing_email}}</td>
                            <td>{{$order->billing_address.','.$order->billing_city.','.$order->billing_province}}</td>
                            <td>{{presentPrice($order->billing_total)}}</td>
                            <td>@foreach ($products as $product)
                                @if($order->id == $product->order_id)
                            {{getProduct($product->product_id).','}}
                            @endif
                            @endforeach</td>
                        </tr>
                            @endif
                        @endforeach
                    </table>
        </div>

    </div>
    <!--coment sections -->


@endsection


@section('extra-js')

@endsection
