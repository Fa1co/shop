<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Falco Shop</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:300,400,700" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">

    </head>
    <body>
        <header class="with-background">
            <div class="top-nav container">
                <div class="logo">Falco Shop</div>
                <ul>
                    <li><a href="{{route('shop.index')}}">{{trans('lang.Shop')}}</a></li>
                    <li><a href="#">{{trans('lang.About us')}}</a></li>
                    <li><a href="{{route('post.index')}}">Blog</a></li>

                                    <!-- Authentication Links -->
                                    @guest
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('login') }}">{{trans('lang.Login')}}</a>
                                        </li>
                                        @if (Route::has('register'))
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ route('register') }}">{{trans('lang.Register')}}</a>
                                            </li>
                                        @endif
                                    @else
                                        <li class="nav-item">
                                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="{{route('profile')}}" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                                {{ Auth::user()->name }} <span class="caret"></span>
                                            </a>
                                        </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ route('logout') }}"
                                                   onclick="event.preventDefault();
                                                                 document.getElementById('logout-form').submit();">{{trans('lang.Logout')}}</a>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                            </li>
                                        </li>
                                    @endguest


                    <li><a href="{{route("cart.index")}}">{{trans('lang.Cart')}} <span class="cart-count"><span>
                                    @if (Cart::instance('default')->count()>0)
                                        {{Cart::instance('default')->count()}}
                                    @endif
                                </span></span> </a></li>
                </ul>
            </div> <!-- end top-nav -->
            <div class="hero container">
                <div class="hero-copy">
                    <h1>Life ain't always beautiful, but it's a beautiful ride.</h1>
                    {{--<div class="hero-buttons">--}}
                        {{--<a href="#" class="button button-white">Blog Post</a>--}}
                        {{--<a href="#" class="button button-white">GitHub</a>--}}
                    {{--</div>--}}
                </div> <!-- end hero-copy -->

                {{--<div class="hero-image">--}}
                    {{--<img src="img/macbook-pro-laravel.png" alt="hero image">--}}
                {{--</div> <!-- end hero-image -->--}}
            </div> <!-- end hero -->
        </header>

        <div class="featured-section">  

            <div class="container">
                <h1 class="text-center">Hello friend</h1>

                <p class="section-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore vitae nisi, consequuntur illum dolores cumque pariatur quis provident deleniti nesciunt officia est reprehenderit sunt aliquid possimus temporibus enim eum hic.</p>

                <div class="text-center button-container">
                    <a href="#" class="button">{{trans('lang.Featured')}}</a>
                </div>

                {{-- <div class="tabs">
                    <div class="tab">
                        Featured
                    </div>
                    <div class="tab">
                        On Sale
                    </div>
                </div> --}}

                <div class="products text-center">
                    @foreach($products as $product)
                        <div class="product">
                            <a href="{{route('shop.show',$product->slug)}}"><img src="{{productImage($product->image)}}" alt="product"></a>
                            <a href="{{route('shop.show',$product->slug)}}"><div class="product-name">{{ $product->name }}</div></a>
                            <div class="product-price">{{ $product->presentPrice() }}</div>
                        </div>
                    @endforeach

                </div> <!-- end products -->

                <div class="text-center button-container">
                    <a href="{{route('shop.index')}}" class="button">{{trans('lang.View more products')}}</a>
                </div>

            </div> <!-- end container -->

        </div> <!-- end featured-section -->

        <div class="blog-section">
            <div class="container">
                <h1 class="text-center">From Our Blog</h1>

                <p class="section-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore vitae nisi, consequuntur illum dolores cumque pariatur quis provident deleniti nesciunt officia est reprehenderit sunt aliquid possimus temporibus enim eum hic.</p>
                <div class="blog-posts">
                @foreach($posts as $post)
                    <div class="blog-post">
                        <a href="{{route('post.show',$post->slug)}}"><img src="{{productImage($post->image)}}" alt="Blog Image"></a>
                        <a href="{{route('post.show',$post->slug)}}"><h2 class="blog-title">{{$post->title}}</h2></a>
                        <div class="blog-description">{{$post->meta_description}}</div>
                    </div>
                @endforeach
                </div>
            </div> <!-- end container -->
        </div> <!-- end blog-section -->

        @include('partials.footer')


    </body>
</html>
