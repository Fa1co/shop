

    <div class="comments">
        <spase>
            <h1 class="product-section-title"> {{trans('lang.Comments')}}</h1>
        </spase>

        @foreach($comments as $coment)
            @if (auth()->user())

                @if(auth()->user()->id == $coment->user_id)
                    <div class="containers">
                        <H4><b>{!! getUserName($coment->user_id)  !!}</b></H4>
                        {{trans('lang.commented:')}}
                        <p>{!! $coment->content !!}  </p>
                        <label class="text-right"> Created at: {!! $coment->created_at !!}</label><br>

                        <form action="{{ route('comment.destroy', $coment->id) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="cart-options">{{trans('lang.Delete comment')}}</button>
                        </form>
                        {{--<a  href="{{route('comment.destroy')}}">Delete comment</a>--}}
                    </div>
                @else
                    <div class="containers">
                        <H4><b>{!! getUserName($coment->user_id)  !!}</b></H4>
                        {{trans('lang.commented:')}}
                        <p>{!! $coment->content !!}  </p>
                        <label class="text-right"> Created at: {!! $coment->created_at !!}</label>
                    </div>
                @endif

            @else
                <div class="containers">
                    <H4><b>{!! getUserName($coment->user_id)  !!}</b></H4>
                    {{trans('lang.commented:')}}
                    <p>{!! $coment->content !!}  </p>
                    <label class="text-right"> Created at: {!! $coment->created_at !!}</label>
                </div>

            @endif

        @endforeach
        {{--{{ $comments->appends(request()->input())->links() }}--}}
        {{$comments->render()}}

    </div>



