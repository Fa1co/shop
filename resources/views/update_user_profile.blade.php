@extends('layout')

@section('title',  auth()->user()->name )

@section('extra-css')



@endsection

@section('content')

    @if(count($errors)>0)
        <div class="alert alert-danger" role="alert">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="products-section container">

        <div class="product-section-information">
            {{--<div>--}}
                {{--<h2>My Profile</h2>--}}
                <div >
                    {{--<img src="{{ productImage(auth()->user()->avatar) }}" alt="product" class="active" id="currentImage">--}}
                </div>
            </div>
            <form action="{{route('update_user_profile.update')}}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <h2>Change some data</h2>
                <div class="form-group">
                    <label>Avatar:</label>
                    <input type="file" name="avatar" class="form-control">
                </div>
                <label for="email">{{trans('lang.Email Address')}}</label>

                <div class="form-group">
                    <input type="email" class="form-control" id="email" name="email" value="{{ auth()->user()->email  ? auth()->user()->email :old('email') }}" required>
                </div>
                <div class="form-group">
                    <label for="name">{{trans('lang.Name')}}</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{  auth()->user()->name  ? auth()->user()->name : old('name') }}" required>
                </div>

                <div class="form-group">
                    <label for="address">{{trans('lang.Address')}}</label>
                    <input type="text" class="form-control" id="address" name="address" value="{{ auth()->user()->address  ? auth()->user()->address :old('address') }}" required>
                </div>

                <div class="half-form">
                    <div class="form-group">
                        <label for="city">{{trans('lang.City')}}</label>
                        <input type="text" class="form-control" id="city" name="city" value="{{ auth()->user()->city  ? auth()->user()->city :old('city') }}" required>
                    </div>
                    <div class="form-group">
                        <label for="province">{{trans('lang.Province')}}</label>
                        <input type="text" class="form-control" id="province" name="province" value="{{ auth()->user()->province  ? auth()->user()->province :old('province') }}" required>
                    </div>
                </div> <!-- end half-form -->

                <div class="half-form">
                    <div class="form-group">
                        <label for="postalcode">{{trans('lang.Postal Code')}}</label>
                        <input type="number" class="form-control" id="postalcode" name="postalcode" value="{{ auth()->user()->postal_code  ? auth()->user()->postal_code : old('postalcode') }}" required>
                    </div>
                    <div class="form-group">
                        <label for="phone">{{trans('lang.Phone')}}</label>
                        <input type="number" class="form-control" id="phone" name="phone" value="{{ auth()->user()->phone  ? auth()->user()->phone :old('phone') }}" required>
                    </div>
                </div> <!-- end half-form -->

                <button type="submit"  class="button-primary full-width">Update</button>

            </form>


        </div>

    </div>
    <!--coment sections -->

    </div> <!-- end product-section -->


@endsection


@section('extra-js')

@endsection
