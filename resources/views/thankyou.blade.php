@extends('layout')

@section('title', 'Thank You')

@section('extra-css')

@endsection

@section('body-class', 'sticky-footer')

@section('content')

   <div class="thank-you-section">
       <h1>{{trans('lang.Thank you for')}}  <br> {{trans('lang.Your Order!')}}</h1>
       <p>{{trans('lang.A confirmation email was sent')}}</p>
       <div class="spacer"></div>
       <div>
           <a href="{{route('Landing-page')}}" class="button">{{trans('lang.Home Page')}}</a>
       </div>
   </div>




@endsection
