@extends('layout')

@section('title', $product->name)

@section('extra-css')



@endsection

@section('content')
    <div class="breadcrumbs">
        <div class="container">
            <a href="{{route('Landing-page')}}">{{trans('lang.Home')}}</a>
            <i class="fa fa-chevron-right breadcrumb-separator"></i>
            <a href="{{route('shop.index')}}">{{trans('lang.Shop')}}</a>
            <i class="fa fa-chevron-right breadcrumb-separator"></i>
            <span>{{$product->name}}</span>
        </div>
    </div> <!-- end breadcrumbs -->

    @if(session()->has('success_message'))
        <div class="alert alert-success" role="alert">
            {{session()->get('success_message')}}
        </div>
    @endif


    <div class="product-section container">
        {{--<div class="product-section-image">--}}
            {{--<img src="{{asset('img/products/'.$product->slug.'.png')}}" alt="product">--}}
        <div>
            <div class="product-section-image">
                <img src="{{ productImage($product->image) }}" alt="product" class="active" id="currentImage">
            </div>
            <div class="product-section-images">
                <div class="product-section-thumbnail selected">
                    <img src="{{ productImage($product->image) }}" alt="product">
                </div>





                @if ($product->images)
                    @foreach (json_decode($product->images, true) as $image)
                        <div class="product-section-thumbnail">
                            <img src="{{ productImage($image) }}" alt="product">
                        </div>
                    @endforeach
                @endif
            </div>

            <div id="tag_container">
            @include('presult')
            </div>
            @if (auth()->user())
                <form action="{{route('comment.store')}}" method="post">
                    <div class="containers">
                        {{csrf_field()}}
                        <textarea name="data" id="editor"></textarea>
                        <input type="hidden" name="id" value="{{$product->id}}">
                        <input type="hidden" name="slug" value="{{$product->slug}}">
                    </div>
                    <button type="submit" class="button"> {{trans('lang.Comment')}}</button>
                </form>
            @else
                <p>
                    {{trans('lang.You must be logged in to post a comment.')}}
                </p>
            @endif
        </div>
        {{--</div>--}}
        <div class="product-section-information">
            <h1 class="product-section-title">{{$product->name}}</h1>
            <div class="product-section-subtitle">{{$product->details}}</div>
            <div class="product-section-price">{{$product->presentPrice()}}</div>

            <p>
                {!! $product->description !!}
            </p>



            <!--<a href="#" class="button">Add to Cart</a> -->
            <form action="{{route('cart.store')}}" method="post">
                {{csrf_field()}}
                <input type="hidden" name="id" value="{{$product->id}}">
                <input type="hidden" name="name" value="{{$product->name}}">
                <input type="hidden" name="price" value="{{$product->price}}">
                <button type="submit" class="button button-plain"> {{trans('lang.Add to Cart')}}</button>
            </form>

        </div>
        <!--coment sections -->

    </div> <!-- end product-section -->


    @include('partials.might-like')
@endsection


@section('extra-js')
    <script>
        (function(){
            const currentImage = document.querySelector('#currentImage');
            const images = document.querySelectorAll('.product-section-thumbnail');

            images.forEach((element) => element.addEventListener('click', thumbnailClick));

            function thumbnailClick(e) {
                //currentImage.classList.remove('active');

                //currentImage.addEventListener('transitionend', () => {
                    currentImage.src = this.querySelector('img').src;
                 //   currentImage.classList.add('active');
               // })

               images.forEach((element) => element.classList.remove('selected'));
               this.classList.add('selected');
            }

        })();
    </script>

    <script src="https://cdn.ckeditor.com/ckeditor5/12.2.0/classic/ckeditor.js"></script>

    <script>
        ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
                console.error( error );
            } );
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript">
        $(window).on('hashchange', function() {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 0) {
                    return false;
                }else{
                    getData(page);
                }
            }
        });

        $(document).ready(function()
        {
            $(document).on('click', '.pagination a',function(event)
            {
                event.preventDefault();

                $('li').removeClass('active');
                $(this).parent('li').addClass('active');

                var myurl = $(this).attr('href');
                var page=$(this).attr('href').split('page=')[1];
                getData(page);
            });

        });

        function getData(page){
            $.ajax(
                {
                     url: '?page=' + page,
                    ///url: url,
                    type: "get",
                    datatype: "html",
                }).done(function(data){
                $("#tag_container").empty().html(data);
                location.hash = page;
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                alert('No response from server');
            });
        }
    </script>

@endsection
