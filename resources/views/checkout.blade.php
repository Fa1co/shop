@extends('layout')

@section('title', 'Checkout')

@section('extra-css')
    <script src="https://js.stripe.com/v3/"></script>

@endsection

@section('content')

    <div class="container">


        @if(session()->has('success_message'))
            <div class="alert alert-success" role="alert">
                {{session()->get('success_message')}}
            </div>
        @endif

        @if(count($errors)>0)
            <div class="alert alert-danger" role="alert">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif


        <h1 class="checkout-heading stylish-heading">{{trans('lang.Checkout')}}</h1>
        <div class="checkout-section">
            <div>
                <form action="{{ route('checkout.store') }}" method="POST" id="payment-form">
                    {{ csrf_field() }}
                    <h2>{{trans('lang.Billing Details')}}</h2>


                        <label for="email">{{trans('lang.Email Address')}}</label>

                        <div class="form-group">
                            <input type="email" class="form-control" id="email" name="email" value="{{ auth()->user()->email  ? auth()->user()->email :old('email') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="name">{{trans('lang.Name')}}</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{  auth()->user()->name  ? auth()->user()->name : old('name') }}" required>
                        </div>



                    <div class="form-group">
                        <label for="address">{{trans('lang.Address')}}</label>
                        <input type="text" class="form-control" id="address" name="address" value="{{ auth()->user()->address  ? auth()->user()->address :old('address') }}" required>
                    </div>

                    <div class="half-form">
                        <div class="form-group">
                            <label for="city">{{trans('lang.City')}}</label>
                            <input type="text" class="form-control" id="city" name="city" value="{{ auth()->user()->city  ? auth()->user()->city :old('city') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="province">{{trans('lang.Province')}}</label>
                            <input type="text" class="form-control" id="province" name="province" value="{{ auth()->user()->province  ? auth()->user()->province :old('province') }}" required>
                        </div>
                    </div> <!-- end half-form -->

                    <div class="half-form">
                        <div class="form-group">
                            <label for="postalcode">{{trans('lang.Postal Code')}}</label>
                            <input type="number" class="form-control" id="postalcode" name="postalcode" value="{{ auth()->user()->postal_code  ? auth()->user()->postal_code : old('postalcode') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="phone">{{trans('lang.Phone')}}</label>
                            <input type="number" class="form-control" id="phone" name="phone" value="{{ auth()->user()->phone  ? auth()->user()->phone :old('phone') }}" required>
                        </div>
                    </div> <!-- end half-form -->

                    <div class="spacer"></div>

                    <h2>{{trans('lang.Payment Details')}}</h2>

                    <div class="form-group">
                        <label for="name_on_card">{{trans('lang.Name on Card')}}</label>
                        <input type="text" class="form-control" id="name_on_card" name="name_on_card" value="" required>
                    </div>

                    <div class="form-group">
                        <label for="card-element">
                            {{trans('lang.Credit or debit card')}}
                        </label>
                        <div id="card-element">
                            <!-- a Stripe Element will be inserted here. -->
                        </div>

                        <!-- Used to display form errors -->
                        <div id="card-errors" role="alert"></div>
                    </div>
                    <div class="spacer"></div>

                    <button type="submit" id="complete-order" class="button-primary full-width">{{trans('lang.Complete Order')}}</button>


                </form>
            </div>



            <div class="checkout-table-container">
                <h2>{{trans('lang.Your Order')}}</h2>

                <div class="checkout-table">

                    @foreach(Cart::content() as $item)


                            <div class="checkout-table-row">
                                <div class="checkout-table-row-left">
                                    <img src="{{productImage($item->model->image)}}" alt="item" class="checkout-table-img">
                                    <div class="checkout-item-details">
                                        <div class="checkout-table-item">{{$item->model->name}}</div>
                                        <div class="checkout-table-description">{{$item->model->details}}</div>
                                        <div class="checkout-table-price">{{$item->model->presentPrice()}}</div>
                                    </div>
                                </div> <!-- end checkout-table -->

                                <div class="checkout-table-row-right">
                                    <div class="checkout-table-quantity">{{$item->qty}}</div>
                                </div>
                            </div> <!-- end checkout-table-row -->
                    @endforeach

                </div> <!-- end checkout-table -->

                <div class="checkout-totals">
                    <div class="checkout-totals-left">
                       <label> {{trans('lang.Subtotal')}}</label>  <br>
                        <!--Discount (10OFF - 10%) <br> -->
                        <label>{{trans('lang.Tax (13%)')}}</label>  <br>
                        <span class="checkout-totals-total">{{trans('lang.Total')}}</span>

                    </div>

                    <div class="checkout-totals-right">
                    {{presentPrice(Cart::subtotal())}}<br>
                       <!-- -$750.00 <br>-->
                        {{presentPrice(Cart::tax())}} <br>
                        <span class="checkout-totals-total">{{presentPrice(Cart::total())}}</span>

                    </div>
                </div> <!-- end checkout-totals -->


            </div>

        </div> <!-- end checkout-section -->
    </div>

@endsection


@section('extra-js')
    <script>
        (function () {
// Create a Stripe client.
            var stripe = Stripe('pk_test_5jvhMS7viMfYlhntoKEOTpRI00WbQPOfJU');

// Create an instance of Elements.
            var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
            var style = {
                base: {
                    color: '#32325d',
                    fontFamily: '"Roboto,Helvetica Neue", Helvetica, sans-serif',
                    fontSmoothing: 'antialiased',
                    fontSize: '16px',
                    '::placeholder': {
                        color: '#aab7c4'
                    }
                },
                invalid: {
                    color: '#fa755a',
                    iconColor: '#fa755a'
                }
            };

// Create an instance of the card Element.
            var card = elements.create('card', {
                style: style,
                hidePostalCode:true,
            });

// Add an instance of the card Element into the `card-element` <div>.
            card.mount('#card-element');

// Handle real-time validation errors from the card Element.
            card.addEventListener('change', function(event) {
                var displayError = document.getElementById('card-errors');
                if (event.error) {
                    displayError.textContent = event.error.message;
                } else {
                    displayError.textContent = '';
                }
            });

            var form = document.getElementById('payment-form');
            form.addEventListener('submit', function(event) {
                event.preventDefault();

                // Disable the submit button to prevent repeated clicks
                document.getElementById('complete-order').disabled = true;

                var options = {
                    name: document.getElementById('name_on_card').value,
                    address_line1: document.getElementById('address').value,
                    address_city: document.getElementById('city').value,
                    address_state: document.getElementById('province').value,
                    address_zip: document.getElementById('postalcode').value
                }

                stripe.createToken(card, options).then(function(result) {
                    if (result.error) {
                        // Inform the user if there was an error
                        var errorElement = document.getElementById('card-errors');
                        errorElement.textContent = result.error.message;

                        // Enable the submit button
                        document.getElementById('complete-order').disabled = false;
                    } else {
                        // Send the token to your server
                        stripeTokenHandler(result.token);
                    }
                });
            });

// Submit the form with the token ID.
            function stripeTokenHandler(token) {
                // Insert the token ID into the form so it gets submitted to the server
                var form = document.getElementById('payment-form');
                var hiddenInput = document.createElement('input');
                hiddenInput.setAttribute('type', 'hidden');
                hiddenInput.setAttribute('name', 'stripeToken');
                hiddenInput.setAttribute('value', token.id);
                form.appendChild(hiddenInput);

                // Submit the form
                form.submit();
            }        })();
    </script>

    @endsection