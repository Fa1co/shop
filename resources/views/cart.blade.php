@extends('layout')

@section('title', 'Shopping Cart')

@section('extra-css')

@endsection

@section('content')

    <div class="breadcrumbs">
        <div class="container">
            <a href="{{route('Landing-page')}}">{{trans('lang.Home')}}</a>
            <i class="fa fa-chevron-right breadcrumb-separator"></i>
            <span>{{trans('lang.Shopping Cart')}}</span>
        </div>
    </div> <!-- end breadcrumbs -->

    <div class="cart-section container">
        <div>
            @if(session()->has('success_message'))
                <div class="alert alert-success" role="alert">
                    {{session()->get('success_message')}}
                </div>
            @endif

            @if(count($errors)>0)
                    <div class="alert alert-danger" role="alert">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                @if (Cart::count() > 0)

                    <h2>{{ Cart::count() }} {{trans('lang.item(s) in Shopping Cart')}} </h2>
            <div class="cart-table">

                @foreach(Cart::content() as $item)
                <div class="cart-table-row">
                    <div class="cart-table-row-left">
                        <a href="{{route('shop.show', $item->model->slug)}}"><img src="{{productImage($item->model->image)}}" alt="item" class="cart-table-img"></a>
                        <div class="cart-item-details">
                            <div class="cart-table-item"><a href="{{route('shop.show', $item->model->slug)}}">{{$item->model->name}}</a></div>
                            <div class="cart-table-description">{{$item->model->details}}</div>
                        </div>
                    </div>
                    <div class="cart-table-row-right">
                        <div class="cart-table-actions">
                            <form action="{{ route('cart.destroy', $item->rowId) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}

                                <button type="submit" class="cart-options" > {{trans('lang.Remove')}}</button>
                            </form>

                            <form action="{{ route('cart.switchToSaveForLater', $item->rowId) }}" method="POST">
                                {{ csrf_field() }}
                                <button type="submit" class="cart-options">{{trans('lang.Save for Later')}}</button>
                            </form>
                        </div>
                        <div>
                            <select class="quantity" data-id="{{ $item->rowId }}" data-productQuantity="{{ $item->model->quantity }}">
                                @for ($i = 1; $i < 5 + 1 ; $i++)
                                    <option {{ $item->qty == $i ? 'selected' : '' }}>{{ $i }}</option>
                                @endfor

                            </select>
                        </div>
                        <div>{{ presentPrice($item->subtotal) }}</div>
                    </div>
                </div> <!-- end cart-table-row -->
                    @endforeach
            </div> <!-- end cart-table -->

            <div class="cart-totals">
                <div class="cart-totals-left">
                    {{trans('lang.Shipping is free because we’re awesome like that:)')}}
                </div>

                <div class="cart-totals-right">
                    <div>
                        {{trans('lang.Subtotal')}} <br>
                        {{trans('lang.Tax (13%)')}} <br>
                        <span class="cart-totals-total">{{trans('lang.Total')}}</span>
                    </div>
                    <div class="cart-totals-subtotal">
                       {{presentPrice(Cart::subtotal())}} <br>
                        {{presentPrice(Cart::tax())}} <br>
                        <span class="cart-totals-total">{{presentPrice(Cart::total())}}</span>
                    </div>
                </div>
            </div> <!-- end cart-totals -->

            <div class="cart-buttons">
                <a href="{{ route('shop.index') }}" class="button">{{trans('lang.Continue Shopping')}}</a>
                <a href="{{ route('checkout.index') }}" class="button-primary">{{trans('lang.Proceed to Checkout')}}</a>
            </div>

                @else
                    <h3>{{trans('lang.No items in Cart!')}}</h3>
                    <div class="spacer"></div>
                    <a href="{{ route('shop.index') }}" class="button">{{trans('lang.Continue Shopping')}}</a>
                    <div class="spacer"></div>
                    @endif



                @if (Cart::instance('saveForLater')->count() > 0)

                    <h2>{{ Cart::instance('saveForLater')->count() }} {{trans('lang.item(s) Saved For Later')}}</h2>


            <div class="saved-for-later cart-table">
                @foreach(Cart::instance('saveForLater')->content() as $item)


                <div class="cart-table-row">
                    <div class="cart-table-row-left">
                        <a href="{{route('shop.show', $item->model->slug)}}"><img src="{{ productImage($item->model->image)}}" alt="item" class="cart-table-img"></a>
                        <div class="cart-item-details">
                            <div class="cart-table-item"><a href="{{route('shop.show', $item->model->slug)}}">{{$item->model->name}}</a></div>
                            <div class="cart-table-description">1{{$item->model->details}}</div>
                        </div>
                    </div>
                    <div class="cart-table-row-right">
                        <div class="cart-table-actions">
                            <form action="{{ route('saveForLater.destroy', $item->rowId) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="cart-options">{{trans('lang.Remove')}}</button>
                            </form>

                            <form action="{{ route('saveForLater.switchToCart', $item->rowId) }}" method="POST">
                                {{ csrf_field() }}
                                <button type="submit" class="cart-options">{{trans('lang.Move to cart')}}</button>
                            </form>
                        </div>
                        {{-- <div>
                            <select class="quantity">
                                <option selected="">1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div> --}}
                        <div>{{$item->model->presentPrice()}}</div>
                    </div>
                </div> <!-- end cart-table-row -->
                @endforeach
                @else
                    <h3>{{trans('lang.You have no items saved for later')}}</h3>

                @endif


            </div> <!-- end saved-for-later -->

        </div>

    </div> <!-- end cart-section -->

    @include('partials.might-like')


@endsection

@section('extra-js')
    <script src="{{ asset('js/app.js') }}"></script>
    <script defer>
        (function(){
            const classname = document.querySelectorAll('.quantity')

            Array.from(classname).forEach(function(element) {
                element.addEventListener('change', function() {
                    const id = element.getAttribute('data-id')
                    const productQuantity = element.getAttribute('data-productQuantity')

                    axios.patch(`/cart/${id}`, {
                        quantity: this.value,
                        productQuantity: productQuantity
                    })
                        .then(function (response) {
                            // console.log(response);
                            window.location.href = '{{ route('cart.index') }}'
                        })
                        .catch(function (error) {
                            // console.log(error);
                            window.location.href = '{{ route('cart.index') }}'
                        });
                })
            })
        })();
    </script>

    @endsection