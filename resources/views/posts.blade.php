@extends('layout')

@section('title',  'Blog' )

@section('extra-css')



@endsection

@section('content')

    <div class="breadcrumbs">
        <div class="container">
            <a href="{{route('Landing-page')}}">{{trans('lang.Home')}}</a>
            <i class="fa fa-chevron-right breadcrumb-separator"></i>
            <span>Blog</span>
        </div>
    </div>

    @if(session()->has('success_message'))
        <div class="alert alert-success" role="alert">
            {{session()->get('success_message')}}
        </div>
    @endif




    <div class="blog-section">
        <div class="container">
            <h1 class="text-center">From Our Blog</h1>
            <div class="blog-posts">
                @foreach($posts as $post)
                    <div class="blog-post">
                        <a href="{{route('post.show',$post->slug)}}"><img src="{{productImage($post->image)}}" alt="Blog Image"></a>
                        <a href="{{route('post.show',$post->slug)}}"><h2 class="blog-title">{{$post->title}}</h2></a>
                        <div class="blog-description">{{$post->meta_description}}</div>
                    </div>
                @endforeach
                {!! $posts->render() !!}
            </div>
        </div> <!-- end container -->
    </div> <!-- end blog-section -->


@endsection


@section('extra-js')

@endsection
