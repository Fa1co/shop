@extends('layout')

@section('title',  auth()->user()->name )

@section('extra-css')



@endsection

@section('content')
    <div class="breadcrumbs">
        <div class="container">
            <a href="{{route('Landing-page')}}">{{trans('lang.Home')}}</a>

            <i class="fa fa-chevron-right breadcrumb-separator"></i>
            <span>{{auth()->user()->name}}</span>
        </div>
    </div> <!-- end breadcrumbs -->

    @if(session()->has('success_message'))
        <div class="alert alert-success" role="alert">
            {{session()->get('success_message')}}
        </div>
    @endif

    <div class="products-section container">
                <div class="sidebar">
                    <ul>
                        <li><a href="{{route('profile')}}">My profile</a></li>
                        <li><a href="{{route('orders')}}">My orders</a></li>
                    </ul>
                </div> <!-- end sidebar -->


                            <div class="products text-center">
                                <div>
                                    <h2>My Profile</h2>
                                    <div class="product-section-image">
                                        <img src="{{ productImage(auth()->user()->avatar) }}" alt="product" class="active" id="currentImage">
                                    </div>
                                </div>
                                <div class="product-section-information">
                                            <label for="email" >{{trans('lang.Email Address')}}:</label>
                                            <div class="form-group">
                                                <Label  class="form-control">{{ auth()->user()->email }}</Label>
                                            </div>
                                            <div class="form-group">
                                                <label for="email" >{{trans('lang.Name')}}:</label>
                                                <Label  class="form-control">{{ auth()->user()->name }}</Label>
                                            </div>

                                        <div class="form-group">
                                            <label for="email" >{{trans('lang.Address')}}:</label>
                                            <Label  class="form-control">{{ auth()->user()->address ? auth()->user()->address : 'none' }}</Label>

                                        </div>

                                        <div class="half-form">
                                            <div class="form-group">
                                                <label for="email" >{{trans('lang.City')}}:</label>
                                                <Label  class="form-control">{{ auth()->user()->city ? auth()->user()->city : 'none'}}</Label>

                                            </div>
                                            <div class="form-group">
                                                <label for="email" >{{trans('lang.Province')}}:</label>
                                                <Label  class="form-control">{{ auth()->user()->province  ? auth()->user()->province : 'none' }}</Label>
                                            </div>
                                        </div> <!-- end half-form -->

                                        <div class="half-form">
                                            <div class="form-group">
                                                <label for="email" >{{trans('lang.Postal Code')}}:</label>
                                                <Label  class="form-control">{{ auth()->user()->postal_code  ? auth()->user()->postal_code : 'none' }}</Label>

                                            </div>
                                            <div class="form-group">
                                                <label for="email" >{{trans('lang.Phone')}}:</label>
                                                <Label  class="form-control">{{ auth()->user()->phone  ? auth()->user()->phone : 'none' }}</Label>
                                            </div>
                                    </div>
                                    <a class="button" href="{{route('update_user_profile')}}"> Update data</a>
                                </div>


                        </div>

        </div>
        <!--coment sections -->

    </div> <!-- end product-section -->

@endsection


@section('extra-js')

@endsection
