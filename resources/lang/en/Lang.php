<?php
return [

    //Langing-page
    'Shop'         => 'Shop',
    'About us'         => 'About',
    'Login'         => 'Login',
    'Register'         => 'Register',
    'Logout'         => 'Logout',
    'Featured'         => 'Featured',
    'View more products'         => 'View more products',
    'Cart'         => 'Cart',
    //shop-page
    'Home'         => 'Home',
    'By Category'         => 'By Category',
    'Price:'         => 'Price:',
    'Low to High'         => 'Low to High',
    'High to Low'         => 'High to Low',

    //product-page
    'You must be logged in to post a comment.'         => 'You must be logged in to post a comment.',
    'You might also like...'         => 'You might also like...',
    'Comments'         => 'Comments',
    'commented:'         => 'commented:',
    'Delete comment'         => 'Delete comment',
    'Add to Cart'         => 'Add to Cart',
    'Comment'         => 'Comment',

    //cart-page
    'Shopping Cart'         => 'Shopping Cart',
    'item(s) in Shopping Cart'         => 'item(s) in Shopping Cart',
    'Remove'         => 'Remove',
    'Save for Later'         => 'Save for Later',
    'Shipping is free because we’re awesome like that:)'         => 'Shipping is free because we’re awesome like that:)',
    'Subtotal'         => 'Subtotal',
    'Tax (13%)'         => 'Tax (13%)',
    'Total'         => 'Total',
    'Continue Shopping'         => 'Continue Shopping',
    'Proceed to Checkout'         => 'Proceed to Checkout',
    'No items in Cart!'         => 'No items in Cart!',
    'item(s) Saved For Later'         => 'item(s) Saved For Later',
    'Move to cart'         => 'Move to cart',
    'You have no items saved for later'         => 'You have no items saved for later',

    //checkout-page
    'Checkout'         => 'Checkout',
    'Billing Details'         => 'Billing Details',
    'Email Address'         => 'Email Address',
    'Name'         => 'Name',
    'Address'         => 'Address',
    'City'         => 'City',
    'Province'         => 'Province',
    'Postal Code'         => 'Postal Code',
    'Phone'         => 'Phone',
    'Payment Details'         => 'Payment Details',
    'Name on Card'         => 'Name on Card',
    'Credit or debit card'         => 'Credit or debit card',
    'Complete Order'         => 'Complete Order',
    'Your Order'         => 'Your Order',


    //thankyou-page

    'Thank you for'         => 'Thank you for',
    'Your Order!'         => 'Your Order!',
    'A confirmation email was sent'         => 'A confirmation email was sent',
    'Home Page'         => 'Home Page',

    //cartController
    'Item is already in your cart!'         => 'Item is already in your cart!',
    'Item was added to your cart!'         => 'Item was added to your cart!',
    'Quantity was updated successfully!'         => 'Quantity was updated successfully!',
    'Item has been removed!'         => 'Item has been removed!',
    'Item is already save for Later!'         => 'Item is already save for Later!',
    'Item has been saved for later!'         => 'Item has been saved for later!',
    //checkout-controller
    'Thank you! Your payment has been successfully accepted!'         => 'Thank you! Your payment has been successfully accepted!',
    //saveForLaterController
    'Item has been move to card!'         => 'Item has been move to card!',
    //commentsController
    'Comment has been upload!'         => 'Comment has been upload!',
    'Comment has been removed!'         => 'Comment has been removed!',

];