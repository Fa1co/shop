<?php
return [
    //Langing-page
    'Shop'             => 'Крамниця',
    'About us'         => 'Про нас',
    'Login'         => 'Увійти',
    'Register'         => 'Зареєструватися',
    'Logout'         => 'Вийти',
    'Featured'         => 'Рекомендовані',
    'View more products'         => 'Переглянути більше товарів',
    'Cart'         => 'Кошик',

    //shop-page
    'Home'         => 'Головна',
    'By Category'         => 'За категоріями',
    'Price:'         => 'Ціна:',
    'Low to High'         => 'За зростанням',
    'High to Low'         => 'По спаданню',

    //product-page
    'You must be logged in to post a comment.'         => 'Ви повинні увійти, щоб залишити коментар.',
    'You might also like...'         => 'Вам також може сподобатися ...',
    'Comments'         => 'Коментарі',
    'commented:'         => 'прокоментував:',
    'Delete comment'         => 'Видалити коментар',
    'Add to Cart'         => 'Додати в кошик',
    'Comment'         => 'Прокоментувати',

    //cart-page
    'Shopping Cart'         => 'Кошик',
    'item(s) in Shopping Cart'         => 'пункт (и) в кошик',
    'Remove'         => 'Видалити',
    'Save for Later'         => 'Зберегти на майбутнє',
    'Shipping is free because we’re awesome like that:)'         => 'Доставка безкоштовна, тому що ми настільки чудові :)',
    'Subtotal'         => 'Підсумок',
    'Tax (13%)'         => 'Податок (13%)',
    'Total'         => 'Всього',
    'Continue Shopping'         => 'Продовжити покупки',
    'Proceed to Checkout'         => 'Оформити замовлення',
    'No items in Cart!'         => 'У кошику немає товарів!',
    'item(s) Saved For Later'         => 'пункт (и) збережено для пізніше',
    'Move to cart'         => 'Перемістити до кошика',
    'You have no items saved for later'         => 'У вас немає елементів, збережених майбутнє',

    //checkout-page
    'Checkout'         => 'Провірка',
    'Billing Details'         => 'Платіжні реквізити',
    'Email Address'         => 'Адреса електронної пошти',
    'Name'         => 'Имя',
    'Address'         => 'Адрес',
    'City'         => 'Місто',
    'Province'         => 'Область',
    'Postal Code'         => 'Поштовий індекс',
    'Phone'         => 'Телефон',
    'Payment Details'         => 'Деталі платежу',
    'Name on Card'         => 'Ім\'я на картці',
    'Credit or debit card'         => 'Кредитна або дебетова картка',
    'Complete Order'         => 'Підтвердити замовлення',
    'Your Order'         => 'Ваше замовлення',

    //thankyou-page
    'Thank you for'         => 'Дякую за',
    'Your Order!'         => 'Ваше замовлення!',
    'A confirmation email was sent'         => 'Надіслано підтвердження електронною поштою',
    'Home Page'         => 'Домашня сторінка',

    //cartController
    'Item is already in your cart!'         => 'Товар вже у вашому кошику!',
    'Item was added to your cart!'         => 'Товар додано до кошика!',
    'Quantity was updated successfully!'         => 'Кількість було успішно оновлено!',
    'Item has been removed!'         => 'Товар видалено!',
    'Item is already save for Later!'         => 'Товар уже збережено для пізніше!',
    'Item has been saved for later!'         => 'Товар збережено надалі!',
    //checkout-controller
    'Thank you! Your payment has been successfully accepted!'         => 'Дякую! Ваш платіж успішно прийнято!',
    //saveForLaterController
    'Item has been move to card!'         => 'Товар був перенесений на карту!',
    //commentsController
    'Comment has been upload!'         => 'Коментар був завантажений!',
    'Comment has been removed!'         => 'Коментар був видалений!',

];