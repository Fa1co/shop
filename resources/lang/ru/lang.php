<?php

return [
    //Langing-page
    'Shop'             => 'Магазин',
    'About us'         => 'О нас',
    'Login'         => 'Логин',
    'Register'         => 'Зарегистрироваться',
    'Logout'         => 'Выйти',
    'Featured'         => 'Рекомендуемые',
    'View more products'         => 'Посмотреть больше продуктов',
    'Cart'         => 'Корзина',

    //shop-page
    'Home'         => 'Главная',
    'By Category'         => 'По категориям',
    'Price:'         => 'Цена:',
    'Low to High'         => 'По возрастанию',
    'High to Low'         => 'По убыванию',

    //product-page
    'You must be logged in to post a comment.'         => 'Вы должны войти, чтобы оставить комментарий.',
    'You might also like...'         => 'Вам также может понравиться ...',
    'Comments'         => 'Комментарии',
    'commented:'         => 'прокомментировал:',
    'Delete comment'         => 'Удалить комментарий',
    'Add to Cart'         => 'Добавить в корзину',
    'Comment'         => 'Прокомментировать',

    //cart-page
    'Shopping Cart'         => 'Корзина',
    'item(s) in Shopping Cart'         => 'товар (ы) в корзине',
    'Remove'         => 'Удалить',
    'Save for Later'         => 'Сохранить на потом',
    'Shipping is free because we’re awesome like that:)'         => 'Доставка бесплатная, потому что мы такие классные :)',
    'Subtotal'         => 'Промежуточный итог',
    'Tax (13%)'         => 'Налог (13%)',
    'Total'         => 'Всего',
    'Continue Shopping'         => 'Продолжить покупки',
    'Proceed to Checkout'         => 'Оформить заказ',
    'No items in Cart!'         => 'Нет товаров в корзине!',
    'item(s) Saved For Later'         => 'товар(ы) сохранены на потом',
    'Move to cart'         => 'Переместить в корзину',
    'You have no items saved for later'         => 'У вас нет предметов, сохраненных на потом',

    //checkout-page
    'Checkout'         => 'Проверка',
    'Billing Details'         => 'Платежные реквизиты',
    'Email Address'         => 'Адрес электронной почты',
    'Name'         => 'Имя',
    'Address'         => 'Адрес',
    'City'         => 'Город',
    'Province'         => 'Область',
    'Postal Code'         => 'Почтовый индекс',
    'Phone'         => 'Телефон',
    'Payment Details'         => 'Детали платежа',
    'Name on Card'         => 'Имя на карте',
    'Credit or debit card'         => 'Кредитная или дебетовая карта',
    'Complete Order'         => 'Завершить заказ',
    'Your Order'         => 'Ваш заказ',

    //thankyou-page

    'Thank you for'         => 'Спасибо  за',
    'Your Order!'         => 'Ваш заказ!',
    'A confirmation email was sent'         => 'Письмо с подтверждением было отправлено',
    'Home Page'         => 'Домашняя страница',

    //cartController
    'Item is already in your cart!'         => 'Товар уже в вашей корзине!',
    'Item was added to your cart!'         => 'Товар добавлен в вашу корзину!',
    'Quantity was updated successfully!'         => 'Количество успешно обновлено!',
    'Item has been removed!'         => 'Товар был удален!',
    'Item is already save for Later!'         => 'Товар уже сохранен на потом!',
    'Item has been saved for later!'         => 'Товар был сохранен на потом!',
    //checkout-controller
    'Thank you! Your payment has been successfully accepted!'         => 'Спасибо! Ваш платеж был успешно принят!',
    //saveForLaterController
    'Item has been move to card!'         => 'Товар был перенесен на карту!',
    //commentsController
    'Comment has been upload!'         => 'Комментарий был загружен!',
    'Comment has been removed!'         => 'Комментарий был удален!',

];