<?php
function presentPrice($price)
{
    return '$'.number_format($price);
}

function setActiveCategory($category, $output="active")
{
    return request()->category == $category ? $output:'';
}

function productImage($path)
{
    return $path && file_exists('storage/'.$path) ? asset('storage/'.$path) : asset('img/img_not_available.png');
}

function getUserName($id)
{
    $user=\App\User::where('id',$id)->get('name')->first();
    return $user->name;
}

function getProduct($id)
{
    $product_name=\App\Product::where('id',$id)->get('details')->first();

    return $product_name->details;
}

