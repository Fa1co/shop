<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostComments extends Model
{
    protected $table = 'post_comments';
    protected $fillable = ['user_id','post_id' ,'email', 'content'];
}
