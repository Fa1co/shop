<?php

namespace App\Http\Controllers;

use App\Comments;
use App\Product;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return view('thankforcomment');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());

        $coment=Comments::create([
            'user_id'=> auth()->user()->id,
            "product_id"=>$request->id,
            "email"=>auth()->user()->email,
            "content"=>$request->data,
        ]);



        $product=Product::where('slug',$request->slug)->firstOrFail();
        $comments=Comments::where('product_id',$request->id)->get();
        $mightAlsoLike=Product::where('slug','!=',$request->slug)->mightAlsoLike()->get();


//        return redirect()->route('comment.index')->with('success_message', 'Thank you! Your payment has been successfully accepted!');
        return redirect()->back()->withInput()->with('success_message', trans('Comment has been upload!'));

//        return view('product')->with([
//            'product'=>$product,
//            'mightAlsoLike'=>$mightAlsoLike,
//            'comments'=>$comments,
//        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

//        $product=Comments::where('id',$id)->first();
        Comments::destroy($id);

        return  back()->with('success_message',trans('Comment has been removed!'));
//          return redirect()->route('shop.index');
       // return redirect()->back()->withInput();
    }

}
