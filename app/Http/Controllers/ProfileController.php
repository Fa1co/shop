<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;


class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try{
            $this->validate($request, [

                'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'email' => 'required|email',

            ]);

            $image = $request->file('avatar');

            $old_image=User::where('id',auth()->user()->id)->first();
            //dd($old_image->avatar);
            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('\storage\users');
            $destinationPathForOldImage = public_path('\storage/');
           //Storage::delete($destinationPath.$old_image);
            //dd($destinationPathForOldImage.$old_image->avatar);
            //File::delete($destinationPathForOldImage,  $old_image->avatar);
            File::delete($destinationPathForOldImage.$old_image->avatar);
            //$old_image->delete($destinationPath.$old_image);
            $image->move($destinationPath, $input['imagename']);

            //dd($request->all());
            User::where('id',auth()->user()->id)->update([
                'name'=>$request->name,
                'email'=>$request->email,
                'avatar'=>'users/'.$input['imagename'],
                'address'=>$request->address,
                'city'=>$request->city,
                'province'=>$request->province,
                'postal_code'=>$request->postalcode,
                'phone'=>$request->phone,
            ]);
            return redirect()->route('profile')->with('success_message', 'Profile updated');
        }catch (CardErrorException $e) {
            return back()->withErrors('Error! ' . $e->getMessage());
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
