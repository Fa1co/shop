<?php

namespace App\Http\Controllers;
use App\Category;
use App\Comments;
use App\Product;
use Illuminate\Http\Request;

class ShopControler extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pagination = 9;
        $categories = Category::all();
        if(request()->category){
            $products = Product::with('categories')->whereHas('categories', function ($query){
               $query->where('slug', request()->category);
            });
            {
                $categories=Category::all();
            $categoryName = optional($categories->where('slug', request()->category)->first())->name;
            }
        }else{
            $products = Product::where('featured', true);
            $categoryName = trans('lang.Featured');
        }


        if (request()->sort == 'low_high') {
            $products = $products->orderBy('price')->paginate(9);
        } elseif (request()->sort == 'high_low') {
            $products = $products->orderBy('price', 'desc')->paginate(9);
        }else {
            $products = $products->paginate(9);
        }

        return view('shop')->with(
            [
                'products'=>$products,
                'categories'=>$categories,
                'categoryName'=> $categoryName ,
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request ,$slug)
    {

       $product=Product::where('slug',$slug)->firstOrFail();
       $comments=Comments::where('product_id',$product->id)->paginate(5);
        $mightAlsoLike=Product::where('slug','!=',$slug)->mightAlsoLike()->get();

        if ($request->ajax()) {


            return view('presult', compact('comments'),[
                'product'=>$product,
                'mightAlsoLike'=>$mightAlsoLike,
            ]);
        }

        return view('product',compact('comments'),[
            'product'=>$product,
            'mightAlsoLike'=>$mightAlsoLike,
        ]);

//       return view('product')->with([
//           'product'=>$product,
//           'mightAlsoLike'=>$mightAlsoLike,
//           'comments'=>$comments,
//           ]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
