<?php
use Gloudemans\Shoppingcart\Facades\Cart;

Route::group(['prefix' => App\Http\Middleware\LocaleMiddleware::getLocale()], function(){
    //Route::view('/', 'landing-page');
    Route::get('/','LandingPageControler@index')->name('Landing-page');

    Route::get('/shop','ShopControler@index')->name('shop.index');

    Route::get('/shop/{product}','ShopControler@show')->name('shop.show');
//Route::view('/shop', 'shop');
    Route::view('/product', 'product');


//Route::get('/product','ShopController@ajaxPagination')->name('ajax.pagination');


    Route::post('/product', 'CommentsController@store')->name('comment.store');
    Route::delete('/product/{comments}','CommentsController@destroy')->name('comment.destroy');


    Route::get('/cart','CartController@index')->name('cart.index');
    Route::post('/cart','CartController@store')->name('cart.store');
    Route::patch('/cart/{product}', 'CartController@update')->name('cart.update');
    Route::delete('/cart/{product}','CartController@destroy')->name('cart.destroy');
    Route::post('/cart/switchToSaveForLater/{product}','CartController@switchToSaveForLater')->name('cart.switchToSaveForLater');

    Route::delete('/saveForLater/{product}','saveForLaterControler@destroy')->name('saveForLater.destroy');
    Route::post('/saveForLater/switchToSaveForLater/{product}','saveForLaterControler@switchToCart')->name('saveForLater.switchToCart');

    Route::get('/empty', function (){
        Cart::instance('saveForLater')->destroy();
    });
//Route::view('/cart', 'cart');



//Route::get('/checkout','CheckoutController@index')->name('checkout.index')->middleware('auth');
    Route::get('/checkout','CheckoutController@index')->name('checkout.index');

    Route::post('/checkout','CheckoutController@store')->name('checkout.store');
//Route::view('/checkout', 'checkout');

    Route::get('/thankyou','ConfirmationController@index')->name('confirmation.index');
//Route::view('/thankyou', 'thankyou');


    Route::group(['prefix' => 'admin'], function () {
        Voyager::routes();
    });

    Auth::routes();

    Route::get('/home', 'HomeController@index')->name('home');


    Route::view('/user_profile', 'user_profile')->name('profile')->middleware('auth');
    Route::view('/update_user_profile', 'update_user_profile')->name('update_user_profile')->middleware('auth');
    Route::post('/update_user_profile', 'ProfileController@store')->name('update_user_profile.update')->middleware('auth');



    Route::get('/user_orders', 'UserOrdersController@index')->name('orders')->middleware('auth');



    Route::get('/posts','PostController@index')->name('post.index');

    Route::get('/posts/{post}','PostController@show')->name('post.show');
    Route::post('/posts', 'PostCommentsController@store')->name('post_comment.store');
    Route::delete('/posts/{comments}','PostCommentsController@destroy')->name('post_comment.destroy');
});

Route::get('setlocale/{lang}', function ($lang) {

    $referer = \Illuminate\Support\Facades\Redirect::back()->getTargetUrl(); //URL предыдущей страницы
    $parse_url = parse_url($referer, PHP_URL_PATH); //URI предыдущей страницы

    //разбиваем на массив по разделителю
    $segments = explode('/', $parse_url);

    //Если URL (где нажали на переключение языка) содержал корректную метку языка
    if (in_array($segments[1], App\Http\Middleware\LocaleMiddleware::$languages)) {

        unset($segments[1]); //удаляем метку
    }

    //Добавляем метку языка в URL (если выбран не язык по-умолчанию)
    if ($lang != App\Http\Middleware\LocaleMiddleware::$mainLanguage){
        array_splice($segments, 1, 0, $lang);
    }

    //формируем полный URL
    $url = \Illuminate\Support\Facades\Request::root().implode("/", $segments);

    //если были еще GET-параметры - добавляем их
    if(parse_url($referer, PHP_URL_QUERY)){
        $url = $url.'?'. parse_url($referer, PHP_URL_QUERY);
    }
    return redirect($url); //Перенаправляем назад на ту же страницу

})->name('setlocale');
