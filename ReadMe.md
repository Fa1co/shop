How to deploy
1. git clone https://Fa1co@bitbucket.org/Fa1co/shop.git
2. Run command - composer install
3. Rename .env.example to .env
4. Import database dump - fshop.sql
5. Set up in env file db connection and stripe api key. 
6. Run command - php artisan serve
After these commands, the site should be accessible by default at <http://localhost:8000>